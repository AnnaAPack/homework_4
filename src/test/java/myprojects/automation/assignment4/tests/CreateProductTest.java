package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseTest;
import myprojects.automation.assignment4.model.ProductData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateProductTest extends BaseTest {

    @DataProvider(name="loginInfo")
    public Object[][] parseLoginData() {
        return new Object[][]{
                {"webinar.test@gmail.com","Xcg7299bnSmMuRLp9ITw"},

        };
    }


    @Test(dataProvider = "loginInfo")
    public void createNewProduct(String login, String password) {
        actions.login(login, password);
        ProductData product = new ProductData("TestProduct_"+ (int) (Math.random()*100), (int) (Math.random()*100), (float) (Math.random()*100));
        actions.createProduct(product);
        actions.checkFront(product);


    }
}
