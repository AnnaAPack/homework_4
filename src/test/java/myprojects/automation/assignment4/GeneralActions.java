package myprojects.automation.assignment4;


import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.utils.Properties;
import myprojects.automation.assignment4.utils.logging.CustomReporter;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private By catalogueLink = By.cssSelector("#subtab-AdminCatalog");
    private By productsLink = By.cssSelector("#subtab-AdminProducts");
    private String productName = "Test";
    private Integer productQty;
    private double productPrice;


    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {

        driver.navigate().to(Properties.getBaseAdminUrl());
        driver.findElement(By.id("email")).sendKeys(login);
        driver.findElement(By.id("passwd")).sendKeys(password);
        driver.findElement(By.name("submitLogin")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("main")));
        Assert.assertTrue("Wrong title", driver.findElement(By.className("page-title")).getText().contains("Пульт"));
    }

    /*
     *create new product
     *ProductData contains name, price, qty
     */
    public void createProduct(ProductData newProduct) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(catalogueLink));
        WebElement catalogueLink = driver.findElement(this.catalogueLink);

        Actions actions = new Actions(driver);

        actions.moveToElement(catalogueLink).clickAndHold(catalogueLink).build().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(productsLink));
        WebElement categoriesLink = driver.findElement(this.productsLink);
        actions.moveToElement(categoriesLink).click().build().perform(); //click "Каталог->Товары"

        By pageTitle = By.cssSelector("h2[class='title']");
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageTitle));

        driver.findElement(By.id("page-header-desc-configuration-add")).click(); //click "Новый товар"

        driver.findElement(By.cssSelector("#form_step1_name_1")).sendKeys(newProduct.getName()); //fill the "Название товара" field

        driver.findElement(By.cssSelector("#form_step1_qty_0_shortcut")).sendKeys(Keys.chord(Keys.CONTROL, "a") + Keys.DELETE+newProduct.getQty()); //clear and fill the "Количество" field

        driver.findElement(By.cssSelector("#form_step1_price_shortcut")).sendKeys(Keys.chord(Keys.CONTROL, "a") + Keys.DELETE+newProduct.getPrice()); //clear and fill of the "Цена" field

        driver.findElement(By.cssSelector("div[class='switch-input ']")).click(); //click the "В сети"/"Не в сети" button
        driver.findElement(By.cssSelector("input[id='submit']")).click(); //click the "Сохранить" button
       // wait.until(ExpectedConditions.alertIsPresent());
    }

    /*
    *check if the created product is displayed in FrontEnd
    * */
    public void checkFront(ProductData newProduct) {
        driver.navigate().to(Properties.getBaseUrl());

        driver.findElement(By.cssSelector("a[class='all-product-link pull-xs-left pull-md-right h4']")).click(); //click the "Все товары" link

        List<WebElement> paginator = driver.findElements(By.cssSelector("a[class='js-search-link']")); //list of page number, displayed in the paginator, excep selected number

        if (paginator.size()>0) //if pages qty>1
        {
            paginator.get(paginator.size()-1).click(); //click the last page number
        }
        By createdProductLink = By.linkText(newProduct.getName());
        wait.until(ExpectedConditions.visibilityOfElementLocated(createdProductLink));
        WebElement createdProduct = driver.findElement(createdProductLink);
        createdProduct.click(); //open the Product Detail page

        //collect the product's displayed properties
        Double displayedProductPrice = Double.valueOf(driver.findElement(By.cssSelector("span[itemprop='price']")).getAttribute("content"));
        String displayedQtyText = driver.findElement(By.xpath("//div[@class='product-quantities']/span")).getText();
        //truncate text, separate the price
        int endPosition = displayedQtyText.indexOf(" ");
        char[] buf = new char[endPosition];
        displayedQtyText.getChars(0,endPosition, buf, 0);
        displayedQtyText=new String(buf);
        int displayedProductQty = Integer.valueOf(displayedQtyText);
        boolean flag = ((displayedProductQty==newProduct.getQty()) && (Math.abs(displayedProductPrice-Float.valueOf(newProduct.getPrice().replaceAll(",",".")))<=0.01));
        if (flag)
        {System.out.println("The product properties are displayed correctly in frontend");
            CustomReporter.logAction("The product properties are displayed correctly in frontend");}
        Assert.assertTrue ("Test failed",flag);
    }
}
